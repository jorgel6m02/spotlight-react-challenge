import { useSearchCoincidences } from "../../hooks/useSearchCoincidence";
import "./spotlight.css";

export default function SearchComponent() {
  const items = [
    { id: 1, name: "Visual studio code" },
    { id: 2, name: "Sublime Text" },
    { id: 3, name: "Intellij IDE" },
    { id: 4, name: "Docker" },
  ];
  const {
    coincidences,
    searchValue,
    setSearchValue,
    handleSearchChange
  } = useSearchCoincidences(items);

  return (
    <div className="center--center">
      <div id="spotlight">
        <div className="input-search__container">
          <input
            type="text"
            placeholder="Search..."
            value={searchValue}
            onChange={handleSearchChange}
          />
          <div className="search-coincidences__container">
            {coincidences?.map(item => (
              <span
                className="search-coincidence__item"
                key={item.id}
                onClick={() => setSearchValue(item.name)}
              >
                {item.name}
              </span>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
