import { useEffect, useState } from "react";
import "./App.css";
import SearchComponent from "./components/Spotlight/SearchComponent";

function App() {
  const [searchIsCalled, setSearchIsCalled] = useState<boolean>(false);

  useEffect(() => {
    window.addEventListener("keydown", (event: KeyboardEvent) => {
      if (event.ctrlKey && event.keyCode === 32) {
        setSearchIsCalled(true);
      }

      if (event.keyCode === 27) {
        setSearchIsCalled(false);
      }
    });
  }, []);

  return (
    <div id="app">
      {searchIsCalled &&
        <SearchComponent />
      }
    </div>
  )
}

export default App
