import { useState, useEffect } from "react";

interface SearchItem {
  id: number,
  name: string,
}

export const useSearchCoincidences = (items: SearchItem[]) => {
  const [coincidences, setCoincidences] = useState<SearchItem[]>([]);
  const [searchValue, setSearchValue] = useState<string>("");

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setSearchValue(event.target.value);

  const findCoincidences = (value: string) => {
    if (value) {
      const filteredCoincidences = items.filter((item: SearchItem) =>
        item.name.toLowerCase().includes(value.toLowerCase())
      );
      setCoincidences(filteredCoincidences);
    } else {
      setCoincidences(items);
    }
  }

  useEffect(() => {
    findCoincidences(searchValue);
  }, [searchValue]);

  return { coincidences, searchValue, setSearchValue, handleSearchChange };
}
